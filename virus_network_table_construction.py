infile1 = open('../../../Downloads/IC9_hicspades_DemoVir_viralverify_v.txt', 'r', encoding='utf8')
infile2 = open('../../../Downloads/IC9_hicspades_DemoVir_viralverify_v_uns.txt', 'r', encoding='utf8')
#infile3 = open('../../../Downloads/DemoVir_assignments_vibrant.txt', 'r', encoding='utf8')
infile4 = open('../../../Downloads/clustering.tsv', 'r', encoding='utf8')
links_2 = open('../../../Downloads/IC9_hicspades_links_count_2.txt', 'r', encoding='utf8')
links = open('../../../Downloads/IC9_hicspades_links_count.txt', 'r', encoding='utf8')

ver = set()
vib = set()
ver_q = set()
vib_q = set()

ver_f = list()
vib_f = list()

#bins = list()
bins = dict()
bin_dict = dict()
virus_dict = dict()

i = 0

for line in infile4:
 #   i += 1
    words = list(line.split())
    bin_dict[words[0]] = int(words[1])
    if int(words[1]) not in bins.keys():
        bins[int(words[1])] = set()
        bins[int(words[1])].add(words[0])
    else:
        bins[int(words[1])].add(words[0])
#print(bins)
    # bins.append(words[0])
   # if i < 47:
        #for word in words:
        #    bin_dict[word] = i

for line in infile1:
    words = line.split()
    ver.add(words[0])
    virus_dict[words[0]] = list(words[1:])
    if float(words[2]) > 50:
        ver_q.add(words[0])
        ver_f.append(words[3])
for line in infile2:
    words = line.split()
    ver.add(words[0])
    virus_dict[words[0]] = list(words[1:])
    if float(words[2]) > 50:
        ver_q.add(words[0])
        ver_f.append(words[3])
#for line in infile3:
#    words = line.split()
#    vib.add(words[0])
#    if float(words[2]) > 50:
#        vib_q.add(words[0])
#        vib_f.append(words[3])

ver_dict = {i: ver_f.count(i) for i in ver_f}
#vib_dict = {i: vib_f.count(i) for i in vib_f}

print("ViralVerify ", *ver_dict.items(), sep='\n')
#print("VIBRANT ", *vib_dict.items(), sep='\n')

# print(set(ver_f))
# print(set(vib_f))

#all = ver & vib
#ver_unq = ver - vib
#vib_unq = vib - ver

#all_q = ver_q & vib_q
#ver_unq_q = ver_q - vib_q
#vib_unq_q = vib_q - ver_q

#print("all_q =", len(all_q), "ver_unq_q =", len(ver_unq_q), "vib_unq_q =", len(vib_unq_q))
#print("all =", len(all), "ver_unq =", len(ver_unq), "vib_unq =", len(vib_unq))
# print("ver_unq =", ver_unq, "vib_unq =", vib_unq)
print(len(bins.keys()))
#print(len(bins[72]&ver_q))

ver_bin = {i+1: list(bins[i+1] & ver_q) for i in range(72) if len(bins[i+1] & ver_q) > 0}
#print(*list(map(lambda x: (x, len(ver_bin[x])), ver_bin.keys())), len(ver_bin), sep='\n')

#ver_unbin = ver_q
#for i in ver_bin.keys():
#    ver_unbin = ver_unbin - set(ver_bin[i])

#print(len(ver_unbin), len(ver_q))
link_bin = dict()
link_cont = list()
link_cont_vir = list()

n = 0
for line in links_2:
    if n % 1000 == 0:
        print(n)
    n += 1
    words = line.split()
    if words[1] in set(bin_dict.keys()):
        if words[2] in set(virus_dict.keys()):
            if words[2] not in link_bin.keys():
                link_bin[words[2]] = dict()
                link_bin[words[2]][bin_dict[words[1]]] = int(words[0])
            elif bin_dict[words[1]] not in link_bin[words[2]].keys():
                link_bin[words[2]][bin_dict[words[1]]] = int(words[0])
            else:
                link_bin[words[2]][bin_dict[words[1]]] += int(words[0])
    if words[2] in set(bin_dict.keys()):
        if words[1] in set(virus_dict.keys()):
            if words[1] not in link_bin.keys():
                link_bin[words[1]] = dict()
                link_bin[words[1]][bin_dict[words[2]]] = int(words[0])
            elif bin_dict[words[2]] not in link_bin[words[1]].keys():
                link_bin[words[1]][bin_dict[words[2]]] = int(words[0])
            else:
                link_bin[words[1]][bin_dict[words[2]]] += int(words[0])
#    if (words[1] in set(virus_dict.keys())) and (words[2] in set(virus_dict.keys())):
#        link_cont_vir.append(list(words))
#    if (words[1] not in set(bin_dict.keys())) and (words[2] not in set(bin_dict.keys())):
#        link_cont.append(list(words))

n = 0
for line in links:
    if n % 1000 == 0:
        print(n)
    n += 1
    words = line.split()
    if (words[1] in set(virus_dict.keys())) and (words[2] in set(virus_dict.keys())):
        link_cont_vir.append(list(words))


outfile = open('../../../Downloads/IC9_links_vir_bin_hicspades.txt', 'w', encoding='utf8')
outfile_1 = open('../../../Downloads/IC9_links_vir_cont_hicspades.txt', 'w', encoding='utf8')

for item in link_cont_vir:
    print(*item, file=outfile_1)

for key in link_bin.keys():
    for k_item in link_bin[key].keys():
        print(key, k_item, link_bin[key][k_item], virus_dict[key][0], virus_dict[key][1], virus_dict[key][2], virus_dict[key][3], file=outfile, sep="\t")

#print(link_bin[words[2]])

