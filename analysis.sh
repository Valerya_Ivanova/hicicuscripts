#!/bin/bash
source /home/ivanova/anaconda3/etc/profile.d/conda.sh

SAMPLE_NAME=''
IN_FASTQ_R1=
IN_FASTQ_R2=
IN_Hi_C_R1=
IN_Hi_C_R2=

N_THREADS_MULTI=8

USE_MEGAHIT=yes
USE_SPADES=no

ASSEMBLY_READS=megahit_result/final.contigs.fa
ENZIME=

ADAPTER_A=AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
ADAPTER_B=AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT

conda activate py37
#----------Quality control---------
mkdir fastqc_out/
fastqc -o fastqc_out/ -f fastq -t $N_THREADS_MULTI $IN_FASTQ_R1 $IN_FASTQ_R2 $IN_Hi_C_R1 $IN_Hi_C_R2

#change quality params if needed
mkdir seqprep
cd seqprep
SeqPrep -f $IN_FASTQ_R1 -r $IN_FASTQ_R2 -1 unmerget1.fastq.gz -2 unmerget2.fastq.gz -s merged.fastq.gz -A $ADAPTER_A -B $ADAPTER_B
cd ..
#additional parameter may include:
#-O <minimum overall base pair overlap with adapter sequence to trim; default = 10>

mkdir trimmomatic
cd trimmomatic
trimmomatic PE -threads $N_THREADS_MULTI -phred33 ../seqprep/unmerget1.fastq.gz ../seqprep/unmerget2.fastq.gz output_forward_paired.fq.gz output_forward_unpaired.fq.gz output_reverse_paired.fq.gz output_reverse_unpaired.fq.gz SLIDINGWINDOW:4:15 MINLEN:115
#LEADING:3 TRAILING:3 
trimmomatic SE -threads $N_THREADS_MULTI -phred33 ../seqprep/merged.fastq.gz output_merged.fastq.gz SLIDINGWINDOW:4:15 MINLEN:80 
cd ..

#----------MetaPhlan profiling-------------
conda activate py27

mkdir metaphlan
cd metaphlan

#-----profiling WGS data------
metaphlan2.py $IN_FASTQ_R1,$IN_FASTQ_R2 --bowtie2out metaplan_prof.bowtie2.bz2 --nproc $N_THREADS_MULTI --input_type fastq -o  Profiled_metagenome_metaphlan.txt --sample_id_key $SAMPLE_NAME

metaphlan2.py metaplan_prof.bowtie2.bz2 --nproc $N_THREADS_MULTI --input_type bowtie2out -o  Profiled_metagenome_metaphlan_s.txt --sample_id_key $SAMPLE_NAME --tax_lev 's' 
metaphlan2.py metaplan_prof.bowtie2.bz2 --nproc $N_THREADS_MULTI --input_type bowtie2out -o  Profiled_metagenome_metaphlan_g.txt --sample_id_key $SAMPLE_NAME --tax_lev 'g' 

#-------profiling Hi-C data-----
metaphlan2.py $IN_Hi_C_R1,$IN_Hi_C_R1 --bowtie2out metaplan_prof_HiC.bowtie2.bz2 --nproc $N_THREADS_MULTI --input_type fastq -o  Profiled_metagenome_metaphlan_HiC.txt --sample_id_key ${SAMPLE_NAME}_HiC

metaphlan2.py metaplan_prof_HiC.bowtie2.bz2 --nproc $N_THREADS_MULTI --input_type bowtie2out -o  Profiled_metagenome_metaphlan_HiC_s.txt --sample_id_key ${SAMPLE_NAME}_HiC --tax_lev 's' 
metaphlan2.py metaplan_prof_HiC.bowtie2.bz2 --nproc $N_THREADS_MULTI --input_type bowtie2out -o  Profiled_metagenome_metaphlan_HiC_g.txt --sample_id_key ${SAMPLE_NAME}_HiC --tax_lev 'g' 
cd ..

#-----------MiCoP profiling--------------
mkdir micop
cd micop

#-------fungi---------
python /home/ivanova/MiCoP/run-bwa.py $IN_FASTQ_R1 $IN_FASTQ_R2 --fungi --output alignments_micop_fungi.sam

python /home/ivanova/MiCoP/compute-abundances.py alignments_micop_fungi.sam --fungi  --output micop_fungi_C22.txt

#--------virus-----------
python /home/ivanova/MiCoP/run-bwa.py $IN_FASTQ_R1 $IN_FASTQ_R2 --virus --output alignments_micop_virus.sam

python /home/ivanova/MiCoP/compute-abundances.py alignments_micop_virus.sam --virus  --output micop_virus_C22.txt
cd ..

#-----------ASSEMBLING---------
#---------assembly by Spades--------
#check memory param -m
if [ $USE_SPADES = 'yes' ];
then
spades.py --meta -1 trimmomatic/output_forward_paired.fq.gz -2 trimmomatic/output_reverse_paired.fq.gz --merged trimmomatic/output_merged.fastq.gz -o spades_out -t $N_THREADS_MULTI -k 61,71,81,91,99,103,107,111,115,119,123,127 -m 140
fi

#---------assembly by Megahit---------
if [ $USE_MEGAHIT = 'yes' ];
then
conda activate py37
megahit -t $N_THREADS_MULTI -1 trimmomatic/output_forward_paired.fq.gz -2 trimmomatic/output_reverse_paired.fq.gz -r trimmomatic/output_merged.fastq.gz -o megahit_result
conda activate py27
fi

#------------BINNING--------------
#-----------WGS binning by Metabat------------
conda activate py37

#------bwa index-----------
bwa index $ASSEMBLY_READS

#--------alignment------
#bwa mem -t $N_THREADS_MULTI -o prepare_for_binning.sam $ASSEMBLY_READS $IN_FASTQ_R1 $IN_FASTQ_R2
bwa mem -t $N_THREADS_MULTI -o prepare_for_binning_p.sam $ASSEMBLY_READS trimmomatic/output_forward_paired.fq.gz trimmomatic/output_reverse_paired.fq.gz
bwa mem -t $N_THREADS_MULTI -o prepare_for_binning_u.sam $ASSEMBLY_READS trimmomatic/output_merged.fastq.gz

#-----sorting and merging in bam---------
samtools sort prepare_for_binning_p.sam -@$N_THREADS_MULTI -o prepare_for_binning_p.bam
samtools sort prepare_for_binning_u.sam -@$N_THREADS_MULTI -o prepare_for_binning_u.bam

samtools merge prepare_for_binning.bam prepare_for_binning_u.bam prepare_for_binning_p.bam
rm prepare_for_binning_*

#------running metabat2---------
metabat2 -i $ASSEMBLY_READS prepare_for_binning.bam -o metabat_out/bin -v -t $N_THREADS_MULTI

#----------MAGs quality check------------
#-----------checkm and gtdbtk-------------
conda activate py27
checkm lineage_wf -f metabat_out/CheckM.txt -t $N_THREADS_MULTI  -x fa metabat_out/ metabat_out/SCG
gtdbtk classify_wf --cpus $N_THREADS_MULTI --genome_dir metabat_out --extension fa --out_dir gtdbtk_out

#----------Hi-C binning by bin3C------------
conda activate py37
/home/ivanova/BBMap/bbduk.sh in1=$IN_Hi_C_R1 in2=$IN_Hi_C_R2 k=23 hdist=1 mink=11 ktrim=r tpe tbo ftm=5 qtrim=r trimq=10 out=hic_paired.fastq.gz

bwa mem -5SP $ASSEMBLY_READS hic_paired.fastq.gz | samtools view -F 0x904 -bS -o hic2ctg_unsorted.bam -
samtools sort -o hic2ctg.bam -n hic2ctg_unsorted.bam
rm hic2ctg_unsorted.bam

/home/ivanova/bin3C/bin/python2 /home/ivanova/bin3C/bin3C.py mkmap -e $ENZIME -v $ASSEMBLY_READS hic2ctg.bam bin3c_out
/home/ivanova/bin3C/bin/python2 /home/ivanova/bin3C/bin3C.py cluster --only-large -v bin3c_out/contact_map.p.gz bin3c_clust

#----------estimation of Hi-C reads fraction--------
qc3C bam --mean-insert 500 --enzyme $ENZIME -t $N_THREADS_MULTI --bam hic2ctg.bam

#-----------checkm bin3c-------------
conda activate py27
checkm lineage_wf -f bin3c_clust/CheckM.txt -t $N_THREADS_MULTI  bin3c_clust/fasta/  bin3c_clust/fasta/SCG 
gtdbtk classify_wf --cpus $N_THREADS_MULTI --genome_dir bin3c_clust/fasta/ --extension fna --out_dir gtdbtk_out_bin3c

#---------------Plasmid_identification_via_Plasflow------------
conda activate plasflow
mkdir Plasflow
PlasFlow.py --input $ASSEMBLY_READS --output Plasflow/plasflow_predictions_assembly.tsv --threshold 0.7

conda activate py37
blastn -db /home/ivanova/plasmid_database/plasmids_db  -query Plasflow/plasflow_predictions.tsv_plasmids.fasta -perc_identity 95  -outfmt "6 qseqid  sseqid qlen evalue  pident length slen mismatch salltitles" -num_threads 8 -word_size 90 -out Plasflow/Plasmids_blast.out  -max_target_seqs 1 -subject_besthit -max_hsps 1 

cut -f'9' Plasflow/Plasmids_blast.out > Plasflow/Plasmids_names.out
sed -i 's/.*| //' Plasflow/Plasmids_names.out
sed -i 's/ .*//' Plasflow/Plasmids_names.out
sort Plasflow/Plasmids_names.out | uniq -c | sort -nr > Plasflow/abundance_genus.txt

#------------Virus_identification_and_annotetion-------------
#-------------Viralverify-------------
/home/ivanova/viralVerify/viralverify.py -f $ASSEMBLY_READS -o viralverify_assembly  --hmm /hdd20tb/bfxitmo/ivart/viralVerify/nbc_hmms.hmm.gz -p
 #--------------Demovir--------------
/home/ivanova/Demovir/demovir.sh viralverify_assembly/Prediction_results_fasta/final.contigs_virus.fasta $N_THREADS_MULTI 

