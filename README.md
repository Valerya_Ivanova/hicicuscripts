# Hi-C ICU scripts #

The repository contains the scripts of the "Hi-C metagenomics reveal dynamics of antibiotic resistance, mobile elements and virulence potential in gut microbiome of chronically critically ill patients" project.

### List of scripts ###

* analysis.sh - main script, contains run commands for analysis steps such as quality filtering, assembly, binning, taxonomic profiling, plasmids and viruses identification.
* run_anvio.sh - additional script for anvi'o running.
* simulate_data.sh - additional script for WGS and Hi-C data simulation
* virus_network_table_construction.py - additional script for constructing virus-contig and virus-MAG network tables.