#!/bin/bash

N_THREADS_MULTI=8

#-------generation of Hi-C data--------
sim3C --profile mycom.txt -n 50000000 -l 150 -e HpaII -m hic metagenome.fasta meta_HiC.fastq
#-------generation of WGS data--------
iss generate --genomes Klebsiella_pneumoniae_HS11286_pl_pKPHS1.fasta Klebsiella_pneumoniae_HS11286_mexB.fasta Pseudomonas_aeruginosa_PAO1.fasta -n 10m --model NovaSeq --output meta_WGS --cpus $N_THREADS_MULTI --abundance_file abundance.txt